using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsSometthing : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Objects;
    public Transform points;
    public float StartSpawnTime;
    public float intervalTime;
    void Start()
    {
        InvokeRepeating("ins_Objs", StartSpawnTime, intervalTime);
    }

    void ins_Objs()
    {
        Instantiate(Objects, points.transform.position, points.transform.rotation);
    }
}
